# CRM Code7

## Summary

1. [&#127919; Goals](#1-goals)
2. [&#128187; Technologies and tools](#2-technologies-and-tools)
3. [&#9881; Environment](#3-environment)
3. [&#128736; Setup](#4-setup)
4. [&#128260; Running application](#5-running-application)
5. [&#128221; Features](#6-features)
6. [&#9989; Tests](#7-tests)
8. [&#128462; Storybook.js](#8-storybook)

<hr style="margin: 1.5rem 0"/>

### 1. Goals
<br/>
This repository is a simples CRM frontend to manage debit of customers.

<hr style="margin: 1.5rem 0"/>

### 2. Technologies and tools


| Tool  | Use case  |
| ----------------- | ------------------------------- |
| Eslint            | To check and standardize code.  |
| Husky             | Realese operation on git hooks  |
| Jest              | Tests                           |
| Prettier          | Format code                     |
| React             | Frontend framework              |
| React Router DOM  | Handle routes                   |
| SCSS              | Preprocessing stylesheet        |
| Storybook         | Create documentation to components |
| Tailwindcss       | Customize CSS                   |
| Vite              | Mount frontend environment      |


<hr style="margin: 1.5rem 0"/>

### 3. Environment

To setup the enviroment you just need copy the file `.env.template` on root folder, rename the copy to `.env` and update your values.


> &#9888; You can use a mocked API to test the application local, runing the command 
`yarn run server` and set the variable `VITE_CODE7_API` to http://localhost:3000


<hr style="margin: 1.5rem 0"/>

### 4. Setup
  > &#9888; Check Enviroment section before this step.

  > &#9888; If you are using docker/docker-compose, skip this step.

  You just need to type:
```
npm install
```
or
```
yarn install
```

<hr style="margin: 1.5rem 0"/>

### 5. Running application

  > &#9888; To this step, you **need** execute the [Setup recomendations](#3-setup).

  > &#9888; Make sure that you have a copy from file `.env.template` renamed to `.env`.

  You can execute the application by two ways:

1.  **Terminal/Prompt**
      ```
      npm dev
      ```
      or 
        
      ```
      yarn dev
      ```
    
2. **Docker/Docker-compose**

    ```
    docker-compose up
    ```

<hr style="margin: 1.5rem 0"/>

### 6. Features

You can realease CRUD operations to debits of customers.


<hr style="margin: 1.5rem 0"/>

### 7. Tests

To run the tests, just type:
```
yarn test
```

<hr style="margin: 1.5rem 0"/>

### 8. Storybook

Storybook.js is a tool that allow you see and interact with components of your application and generate a documentation about them.

To learn more about storybook, you can access [https://storybook.js.org/](https://storybook.js.org/)

To run storybook, just type:

```
npm run storybook
```
or 
```
yarn run storybook
```
