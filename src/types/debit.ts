export interface DebitType{
  idUsuario: number
  nomeUsuario?: string
  percentual?: number
  motivo: string
  valor: number
  id?: number
  criado?: string
}
