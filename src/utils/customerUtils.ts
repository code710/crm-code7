export const calculatePercFromTotal = (): number => {
  return 0
}

export const formatMoney = (value: number): string => {
  const format = new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL'
  })
  return format.format(value)
}
