/* eslint-disable @typescript-eslint/no-unsafe-member-access */
export const sortByField = (item1: any, item2: any): number => {
  if (item1.sum > item2.sum) {
    return -1
  } else if (item1.sum < item2.sum) {
    return 1
  }
  return 0
}
