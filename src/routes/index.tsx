import {
  BrowserRouter,
  Routes,
  Route
} from 'react-router-dom'
import App from '@/App'
import { Dashboard, Debit } from '@/pages'

const Router: React.FC = () => (
  <BrowserRouter>
    <Routes>
      <Route element={<App />}>
        <Route path='/debit/:id' element={<Debit />}/>
        <Route path='/debit' element={<Debit />}/>
        <Route index element={<Dashboard />}/>
      </Route>
    </Routes>
  </BrowserRouter>
)

export default Router
