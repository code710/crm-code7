import { useEffect, useState } from 'react'
import { Pie } from 'react-chartjs-2'
import { data } from '@/utils/chartData'

interface IPieChartProps{
  title?: string
  chartData?: Array<Array<string | number>>
}

export const PieChart: React.FC<IPieChartProps> = ({ title, chartData }) => {
  const [labels, setLabels] = useState<string[]>(['a', 'b', 'c'])
  const [dataValues, setDataValues] = useState<number[]>([10, 20, 30])

  useEffect(() => {
    if (chartData != null) {
      const localLabels: string[] = []
      const localValues: number[] = []
      chartData.forEach(item => {
        localLabels.push(`${item[0]}`)
        localValues.push(+item[1])
      })
      setLabels(localLabels)
      setDataValues(localValues)
    }
  }, [chartData])

  return (
    <div>
      <Pie
        data={{
          labels,
          datasets: [
            {
              ...data.datasets[0],
              label: title,
              data: dataValues
            }
          ]
        }}
        options={{
          plugins: {
            legend: {
              position: 'bottom'
            },
            title: {
              display: true,
              position: 'top'
            }
          }
        }}
      />
    </div>
  )
}

export default PieChart
