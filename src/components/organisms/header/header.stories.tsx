import { ComponentStory, ComponentMeta } from '@storybook/react'
import { MemoryRouter } from 'react-router-dom'
import { Header } from './index'
import './header.scss'

export default {
  title: 'Components/Organisms/Header',
  component: Header
} as ComponentMeta<typeof Header>

export const Primary: ComponentStory<typeof Header> = () => (
  <MemoryRouter>
    <Header />
  </MemoryRouter>
)
