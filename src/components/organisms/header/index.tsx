import { useRef } from 'react'
import { Link } from 'react-router-dom'
import { GiHamburgerMenu } from 'react-icons/gi'
import { AiFillCloseCircle } from 'react-icons/ai'
import './header.scss'

export const Header: React.FC = () => {
  const divIcons = useRef<HTMLDivElement>(null)
  const navRef = useRef<HTMLElement>(null)

  const toogleMenu = (): void => {
    const iconsList = divIcons.current?.querySelectorAll<SVGAElement>('.header-menu-icon')
    iconsList?.forEach(icon => icon.classList.toggle('hidden-menu'))
    navRef.current?.classList.toggle('hidden-nav')
  }

  return (
    <header className='header'>
      <div className='header-title-div'>
        <Link to='/'>
          <h1 className='header-title'>
            CRM Code7
          </h1>
        </Link>

        <div ref={divIcons}>
          <GiHamburgerMenu
            className='header-menu-icon'
            data-icon='menu-closed'
            onClick={toogleMenu}
          />

          <AiFillCloseCircle
            className='header-menu-icon hidden-menu'
            data-icon='menu-open'
            onClick={toogleMenu}
          />
        </div>
      </div>

      <nav className='header-nav hidden-nav' ref={navRef}>
        <Link to='/'>Dashboard</Link>
        <Link to='/debit'>Criar dívida</Link>
      </nav>
    </header>
  )
}

export default Header
