import './footer.scss'

export const Footer: React.FC = () => {
  return (
    <footer className='footer'>
      <p className='footer-p'>
        &copy; Bravi & Code7 <span>2022</span>
      </p>
    </footer>
  )
}

export default Footer
