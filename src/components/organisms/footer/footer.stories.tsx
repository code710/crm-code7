import { ComponentStory, ComponentMeta } from '@storybook/react'
import { MemoryRouter } from 'react-router-dom'
import { Footer } from './index'
import './footer.scss'

export default {
  title: 'Components/Organisms/Footer',
  component: Footer
} as ComponentMeta<typeof Footer>

export const Default: ComponentStory<typeof Footer> = () => (
  <MemoryRouter>
    <Footer />
  </MemoryRouter>
)
