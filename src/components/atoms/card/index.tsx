import { HTMLAttributes, ReactNode } from 'react'
import './card.scss'

interface ICardProps extends HTMLAttributes<HTMLDivElement>{
  children?: ReactNode
}

export const Card: React.FC<ICardProps> = ({ children, ...props }) => {
  return (
    <div {...props} className={`card ${props?.className}`}>
      {children}
    </div>
  )
}

export default Card
