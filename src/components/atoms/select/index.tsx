import { SelectHTMLAttributes } from 'react'
import './select.scss'

interface IOption {
  label: string
  value: string | number
}

interface ISelectProps extends SelectHTMLAttributes<HTMLSelectElement>{
  options?: IOption[]
}

export const Select: React.FC<ISelectProps> = ({ options, ...props }) => {
  return (
    <select className='select' {...props}>
      <option></option>
      {
        options?.map((option, index) => (
          <option key={`${option.value}-${index}`} value={option.value} selected={props.defaultValue === option.value}>
            {option.label}
          </option>
        ))
      }
    </select>
  )
}

export default Select
