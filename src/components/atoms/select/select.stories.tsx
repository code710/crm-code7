import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Select } from './index';

export default {
  title: 'Components/Atoms/Select',
  component: Select,
} as ComponentMeta<typeof Select>;

const Template: ComponentStory<typeof Select> = (args) => <Select {...args} />;

export const Default = Template.bind({});
Default.args = {
  options: [
    {
      label: 'Option 01',
      value: '1'
    },
    {
      label: 'Option 02',
      value: '2'
    },
    {
      label: 'Option 03',
      value: '3'
    },
  ]
};