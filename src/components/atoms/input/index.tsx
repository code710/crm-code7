import { forwardRef, InputHTMLAttributes } from 'react'
import './input.scss'

interface IInputProps extends InputHTMLAttributes<HTMLInputElement>{}

export const Input = forwardRef<HTMLInputElement, IInputProps>(({ ...props }, ref) => {
  return (
    <input {...props} ref={ref} className='input'/>
  )
})

Input.displayName = 'Input'
export default Input
