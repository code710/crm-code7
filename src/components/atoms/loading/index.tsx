import './loading.scss'

export const Loading: React.FC = () => {
  return (
    <div className="spinner-container">
      <div className="loading-spinner"></div>
    </div>
  )
}

export default Loading
