import { ButtonHTMLAttributes, ReactNode } from 'react'
import Loading from '../loading'
import './button.scss'

interface IButtonProps extends ButtonHTMLAttributes<HTMLButtonElement>{
  variant?: 'contained' | 'outlined'
  value?: string
  children?: ReactNode
  loading?: boolean
}

export const Button: React.FC<IButtonProps> = (
  {
    variant = 'contained',
    value,
    children,
    loading = false,
    ...props
  }
) => {
  return (
    <button
      {...props}
      disabled={loading}
      className={`button ${variant} ${props?.className}`}
    >
      {
        loading
          ? <div className='center'><Loading /></div>
          : <>{value}{children}</>
      }
    </button>
  )
}

export default Button
