import { ComponentStory, ComponentMeta } from '@storybook/react';
import { Button } from './index';

export default {
  title: 'Components/Atoms/Button',
  component: Button,
} as ComponentMeta<typeof Button>;

const Template: ComponentStory<typeof Button> = (args) => <Button {...args} />;

export const Contained = Template.bind({});
Contained.args = {
  variant:'contained',
  value: 'Button',
};

export const Outlined = Template.bind({});
Outlined.args = {
  variant:'outlined',
  value: 'Button',
}