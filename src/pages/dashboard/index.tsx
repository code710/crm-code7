import { MouseEvent, useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { BarChart, Button, Card, PieChart } from '@/components'
import { getAllDebits, getCustomers, deleteDebit } from '@/services'
import { DebitType } from '@/types'
import { formatMoney } from '@/utils/customerUtils'
import { sortByField } from '@/utils/sortData'
import './dashboard.scss'

interface ItemDebitProps {
  id: number
  name: string
  sum: number
}

export const Dashboard: React.FC = () => {
  const [debits, setDebits] = useState<DebitType[]>([])
  const [debitsGrouped, setDebitsGrouped] = useState<Record<number, ItemDebitProps>>({})
  const [top3Customers, setTop3Customers] = useState<any[]>([])
  const navigate = useNavigate()

  const getAmount = (value = 0, item: DebitType): number => value + item.valor
  const getCustomer = (): number => new Set(debits.map(debit => debit.idUsuario)).size

  const groupDebitsByClient = (): void => {
    const dict: Record<number, ItemDebitProps> = {}
    for (const item of debits) {
      if (dict[item.idUsuario] !== undefined) {
        dict[item.idUsuario] = {
          ...dict[item.idUsuario],
          sum: +(dict[item.idUsuario].sum) + item.valor
        }
      } else {
        dict[item.idUsuario] = {
          name: item?.nomeUsuario || '',
          id: item.idUsuario,
          sum: item.valor
        }
      }
    }
    setDebitsGrouped(dict)

    // ordening array
    const debitValues = Object.values(debitsGrouped)
    setTop3Customers(
      debitValues.sort(sortByField)?.slice(0, 3)
    )
  }

  const formatPercent = (value: number, total: number): string => {
    return `${Number(value / total * 100).toFixed(2)}%`
  }

  const confirmDelete = (event: MouseEvent<HTMLButtonElement>, debitId: number): void => {
    event.stopPropagation()
    const accept = confirm('Você realmente deseja excluir a dívida selecionada?')
    if (accept) {
      deleteDebit(debitId)
        .then(() => {
          alert('Dívida deletada com sucesso!')
          fetchDebits()
        })
        .catch(() => alert('Erro ao deletar dívida'))
    }
  }

  const fetchDebits = (): void => {
    getAllDebits()
      .then(async ({ data }) => {
        for (const item of data) {
          const { data: userData } = await getCustomers(item.idUsuario)
          item.nomeUsuario = userData[0].name
        }
        setDebits(data)
      })
      .catch(() => alert('Erro ao buscar dívidas'))
  }

  const handleSeeDebitDetails = (debitId: number): void => {
    navigate(`/debit/${debitId}`)
  }

  useEffect(() => {
    if (debits.length > 0) {
      groupDebitsByClient()
    }
  }, [debits])

  useEffect(() => {
    fetchDebits()
  }, [])

  const getDebitGroup = (index: number): ItemDebitProps => {
    try {
      return debitsGrouped[index]
    } catch (error) {
      return {
        id: 0,
        sum: 0,
        name: ''
      }
    }
  }

  return (
    <div className='p-6'>
      <section>
        <h2 className='section-header'>Métricas</h2>
        <div className="div-charts">
          <Card>
            <span className='card-number' id='debitsLength'>{debits.length}</span>
            <label className='card-title' htmlFor='debitsLength'>Dívida(s)</label>
          </Card>
          <Card>
            <span className='card-number' id='debitsLength'>
              {getCustomer()}
            </span>
            <label className='card-title' htmlFor='debitsLength'>Cliente(s)</label>
          </Card>
          <Card>
            <span className='card-number' id='debitsLength'>
              {formatMoney(debits.reduce(getAmount, 0))}
            </span>
            <label className='card-title' htmlFor='debitsLength'>
              Saldo em dívidas
            </label>
          </Card>
        </div>

        <h3 className='section-header'>Gráficos</h3>
        <div className="div-charts">
          <Card className='chart-card'>
            <PieChart
              title='Top 3 clientes mais individados'
              chartData={[...top3Customers.map(
                (item: ItemDebitProps) => [item.name, item.sum]
              )]}
            />
          </Card>
          <Card className='chart-card'>
            <BarChart
              title='Top 3 clientes mais individados'
              chartData={[...top3Customers.map(
                (item: ItemDebitProps) => [item.name, item.sum]
              )]}
            />
          </Card>
        </div>
      </section>

      <h4 className='section-header'>Dívidas</h4>
      <section>
        <Card className='bg-white overflow-x-auto'>
          <table className='tableDashboard'>
            <thead>
              <tr>
                <th>Cliente</th>
                <th>Valor da dívida</th>
                <th>% do total</th>
                <th>Ações</th>
              </tr>
            </thead>
            <tbody>
              {
                debits?.map(debt => (
                  <tr key={debt.id} onClick={() => handleSeeDebitDetails(debt?.id || 0)}>
                    <td>{debt.nomeUsuario}</td>
                    <td>{formatMoney(debt.valor)}</td>
                    <td>
                      {formatPercent(debt.valor, getDebitGroup(debt.idUsuario)?.sum || 1)}
                    </td>
                    <td>
                      <div className='center'>
                        <Button
                          value='excluir'
                          className='btn-error'
                          onClick={(evt) => confirmDelete(evt, debt?.id || 0)}
                        />
                      </div>
                    </td>
                  </tr>
                ))
              }
            </tbody>
          </table>
        </Card>
      </section>
    </div>
  )
}

export default Dashboard
