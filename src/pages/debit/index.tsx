/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { AxiosResponse } from 'axios'
import { FormEvent, useEffect, useRef, useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import { Button, Card, Input, Loading, Select } from '@/components'
import { getCustomers, createDebit, getOneDebit, updateDebit } from '@/services'
import { CustomerType, DebitType } from '@/types'
import './debit.scss'

export const Debit: React.FC = () => {
  const [customers, setCustomers] = useState<CustomerType[]>([])
  const [loading, setLoading] = useState<boolean>(false)
  const [loadDataFromDebit, setLoadDataFromDebit] = useState<boolean>(false)
  const [loadingCustomers, setLoadingCustomers] = useState<boolean>(false)
  const [debitInfo, setDebitInfo] = useState<DebitType>({ idUsuario: 0, motivo: '', valor: 0 })
  const navigate = useNavigate()
  const { id } = useParams()
  const formRef = useRef<HTMLFormElement>(null)

  const handleSubmit = (event: FormEvent): void => {
    event.preventDefault()
    setLoading(true)

    const target = event.target as HTMLFormElement
    const { idUsuario, motivo, valor } = target

    const data = {
      idUsuario: +idUsuario.value,
      motivo: motivo.value,
      valor: +valor.value
    }

    if (id) {
      updateDebit(+id, data)
        .then(() => {
          alert('Dívida atualizada com sucesso')
        })
        .catch(() => alert('Erro ao atualizar dívida'))
        .finally(() => setLoading(false))
    } else {
      createDebit(data)
        .then(() => {
          alert('Dívida criado com sucesso')
          navigate('/')
        }).catch(() => alert('Erro ao criar dívida'))
        .finally(() => setLoading(false))
    }
  }

  useEffect(() => {
    setLoadingCustomers(true)
    getCustomers()
      .then(({ data }: AxiosResponse) =>
        setCustomers(data as CustomerType[])
      )
      .catch(() => alert('Erro ao buscar clientes'))
      .finally(() => setLoadingCustomers(false))

    return () => undefined
  }, [])

  useEffect(() => {
    if (id && !loadDataFromDebit) {
      setLoadingCustomers(true)
      getOneDebit(+id)
        .then(({ data }) => {
          setDebitInfo(data)
          setLoadDataFromDebit(true)
        })
        .catch(() => alert('Erro ao buscar dados'))
        .finally(() => setLoadingCustomers(false))
    }
  }, [id])

  return (
    <div className='p-4 max-w-3xl m-auto'>

      {
        loadingCustomers
          ? (
            <Card className='center bg-zinc-200'>
              <Loading />
            </Card>)
          : (
            <Card className='bg-zinc-200'>
              <form onSubmit={handleSubmit} ref={formRef}>
                <div>
                  <label>
                    Cliente
                    <Select
                      defaultValue={debitInfo.idUsuario}
                      required
                      options={[...customers.map(cust => ({ label: cust.name, value: cust.id }))]}
                      id='idUsuario'
                      name='idUsuario'
                    />
                  </label>
                </div>

                <div>
                  <label>
                    Motivo da dívida
                    <Input
                      id='motivo'
                      name='motivo'
                      required
                      defaultValue={debitInfo.motivo}
                    />
                  </label>
                </div>

                <div>
                  <label>
                    Valor da dívida
                    <Input
                      type='number'
                      id='valor'
                      name='valor'
                      required
                      defaultValue={debitInfo.valor}
                    />
                  </label>
                </div>

                <div className='div-buttons'>
                  <Button
                    value='cancelar'
                    variant='outlined'
                    className='w-full sm:w-1/2'
                    type='reset'
                  />
                  <Button
                    value='salvar'
                    className='w-full sm:w-1/2'
                    loading={loading}
                    type='submit'
                  />
                </div>
              </form>
            </Card>
            )
      }

    </div>
  )
}

export default Debit
