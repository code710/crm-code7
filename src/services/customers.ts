import { AxiosResponse } from 'axios'
import { CustomerType } from '@/types'
import { APIPlaceholder } from './api'

export const getCustomers = async (id?: number): Promise<AxiosResponse<CustomerType[]>> => {
  let url = '/users'
  if (id !== undefined) {
    url += `?id=${id}`
  }
  return await APIPlaceholder.get<CustomerType[]>(url)
}
