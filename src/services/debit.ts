import { AxiosResponse } from 'axios'
import { DebitType } from '@/types'
import { APICode7 } from './api'

export const createDebit = async (data: DebitType): Promise<AxiosResponse> => {
  return await APICode7.post('/divida', data)
}

export const getAllDebits = async (): Promise<AxiosResponse<DebitType[]>> => {
  return await APICode7.get('/divida')
}

export const getOneDebit = async (debitId: number): Promise<AxiosResponse<DebitType>> => {
  return await APICode7.get(`/divida/${debitId}`)
}

export const deleteDebit = async (debitId: number): Promise<AxiosResponse> => {
  return await APICode7.delete(`/divida/${debitId}`)
}

export const updateDebit = async (debitId: number, data: DebitType): Promise<AxiosResponse> => {
  return await APICode7.patch(`/divida/${debitId}`, data)
}
