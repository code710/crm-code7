import axios from 'axios'

export const APIPlaceholder = axios.create({
  baseURL: import.meta.env.VITE_JSONPLACEHOLDER_URL
})

export const APICode7 = axios.create({
  baseURL: import.meta.env.VITE_CODE7_API,
  params: {
    uuid: import.meta.env.VITE_UUID_APICODE7
  }
})
