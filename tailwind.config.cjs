/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{html,ts,tsx}"
  ],
  theme: {
    extend: {
      colors: {
        primary: '#1e2587',
        primaryDark: '#0e0347',
        background: '#f4f4f7',
        error: '#f34545'
      }
    },
  },
  plugins: [],
}
